from pages.objectPage import SearchHelper
import time

def test_yandex_search(browser):
	yandex_main_page = SearchHelper(browser)
	yandex_main_page.go_to_site()
	yandex_main_page.enter_word("Тензор"+"\n")
	yandex_main_page.click_on_the_search_string()
	elen = yandex_main_page.collect_list_of_request()
	time.sleep(1)
	assert elen != 0, 'Поисковые запросы не найдены'
