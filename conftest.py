import pytest
from selenium.webdriver.chrome.service import Service
from selenium import webdriver

@pytest.fixture(scope="session")
def browser():
    driver = webdriver.Chrome(executable_path="./chromedriver/chromedriver.exe")
    yield driver
    driver.quit()