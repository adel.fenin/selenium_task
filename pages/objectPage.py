from .basePage import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains

class YandexSeacrhLocators:

	YANDEX_STRING = (By.CLASS_NAME, "search2__input")
	YANDEX_INPUT = (By.XPATH, "//*[@id='text']")
	SEARCH_REQUEST = (By.XPATH, "/html/body/header/div/div/div[3]/form/div[4]/ul")
	YANDEX_CARTOON = (By.LINK_TEXT, "Картинки")
	CARTOON_TEXT = (By.CLASS_NAME, "MMOrganicSnippet-TitleWrap")
	BUTTON_LIST_CLICK = (By.CLASS_NAME, "CircleButton-Icon")
	CATEGORY_IMAGE_2 = (By.XPATH, "/html/body/div[3]/div[2]/div[1]/div[1]/div/div[1]")
	CATEGORY_IMAGE_1 = (By.XPATH, "/html/body/div[3]/div[2]/div[1]/div/div/div[1]")
	CATEGORY_NAME_2 = (By.XPATH, '/html/body/header/div/div[1]/div[2]/form/div[1]/span/span/input')
	
class SearchHelper(BasePage):

	def enter_word(self, word):
		search_field = self.find_element(YandexSeacrhLocators.YANDEX_INPUT)
		search_field.click()
		search_field.send_keys(word)
		return search_field
	def click_on_the_search_string(self):
		return self.find_element(YandexSeacrhLocators.YANDEX_STRING).click()
	def collect_list_of_request(self):
		all_list = self.find_element(YandexSeacrhLocators.SEARCH_REQUEST)
		countTenzor = 0
		for i in all_list.text.split('\n'):
			if 'тензор' in i.lower():
				countTenzor += 1
		print('Найдено ', countTenzor, ' запросов с Тензор')
		return countTenzor
	def click_on_cartoon(self):
		return self.find_element(YandexSeacrhLocators.YANDEX_CARTOON).click()
	def check_image_url(self):
		url = self.driver.window_handles
		self.driver.switch_to.window(url[1])
		current = self.driver.current_url
		if 'https://yandex.ru/images/' in current:
			print('Мы перешли на страницу https://yandex.ru/images/')
			return current
		else:
			pytest.fail('Мы не находимся на нужной странице!')
	def click_on_category_image_1(self):
		category_name = self.find_element(YandexSeacrhLocators.CATEGORY_IMAGE_1)
		category_name.click()
		return category_name.text
	def comparison_categoty_picture_name(self, category_name):
		if category_name == self.find_element(YandexSeacrhLocators.CATEGORY_NAME_2).get_attribute('value'):
			print("Названия категорий совпадает")
		else:
			pytest.fail('Мы не находимся на нужной категории!')
	def click_on_category_image_2(self):
		return self.find_element(YandexSeacrhLocators.CATEGORY_IMAGE_2).click()
	def find_cartoon_text(self):
		return self.find_element(YandexSeacrhLocators.CARTOON_TEXT).text
	def find_button_list(self):
		return self.find_elements(YandexSeacrhLocators.BUTTON_LIST_CLICK)
	def click_to_the_button_accurate(self, button_list):
		return ActionChains(self.driver).move_to_element(button_list).click().perform()
	def comparison_picture_name(self, cartoon_text):
		if cartoon_text == self.find_element(YandexSeacrhLocators.CARTOON_TEXT).text:
			print('Текст совпадает')
			check_sentence = True
			return check_sentence
		else:
			pytest.fail('Текст не совпадает')
		