from pages.objectPage import SearchHelper
import time

def test_yandex_search(browser):
	yandex_main_page = SearchHelper(browser)
	yandex_main_page.go_to_site()
	yandex_main_page.click_on_cartoon()
	yandex_main_page.check_image_url()
	category_name = yandex_main_page.click_on_category_image_1()
	yandex_main_page.comparison_categoty_picture_name(category_name)
	yandex_main_page.click_on_category_image_2()
	cartoon_text = yandex_main_page.find_cartoon_text()
	button_list = yandex_main_page.find_button_list()
	yandex_main_page.click_to_the_button_accurate(button_list[3])
	yandex_main_page.click_to_the_button_accurate(button_list[0])
	check_sentence = yandex_main_page.comparison_picture_name(cartoon_text)
	time.sleep(1)
	assert check_sentence == True, 'Название картинок не совпадают'
